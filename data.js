const fs = require('fs')
const csv = require('fast-csv')
let extract = []
fs.createReadStream('weather.dat')
  .pipe(csv.parse({ headers: false }))
  .on('data', row => {
      if(row.length>=1)
        extract.push(row[0].split(' '))
}).on('end', () => {
    filtered = []
    for (let i = 0;  i< extract.length; ++i) {
        filtered[i] = []
        for (let j=0;j<extract[i].length; j++){
            if(extract[i][j] != '')
            filtered[i].push(extract[i][j])
        }
    }
    console.log(filtered)
})