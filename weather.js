const readingDatFile = require('./readingDatFile.js')

class weather extends readingDatFile{
    constructor(file, rowDiff1, rowDiff2, ansRow){
        super(file, rowDiff1, rowDiff2, ansRow)
    }
}

// x = new weather('weather.dat', 1, 2, 0)
// x.data()

module.exports = weather