const readingDatFile = require('./readingDatFile.js')
const football = require('./football.js')
const weather = require('./weather.js')

let minTempSpread = new weather('weather.dat', 1, 2, 0)
minTempSpread.data()

let minGoalDiff = new football('football.dat', 6, 8, 1)
minGoalDiff.data()