const readingDatFile = require('./readingDatFile.js')

class football extends readingDatFile{
    constructor(file, rowDiff1, rowDiff2, ansRow){
        super(file, rowDiff1, rowDiff2, ansRow)
    }
}

// x = new football('football.dat', 6, 8, 1)
// x.data()

module.exports = football