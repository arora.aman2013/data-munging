const fs = require('fs')
const csv = require('fast-csv')

class readingDatFile{
    constructor(file, rowDiff1, rowDiff2, ansRow){
        this.fileName = file
        this.extract = []
        this.rowDiff1 = rowDiff1
        this.rowDiff2 = rowDiff2
        this.ansRow = ansRow
    }
    data = () => {
        fs.createReadStream(this.fileName)
        .pipe(csv.parse({ headers: false }))
        .on('data', row => {
            if(row.length>=1)
                this.extract.push(row[0].split(' '))
        }).on('end', () => {
            let filtered = []
            for (let i = 0;  i< this.extract.length; ++i) {
                filtered[i] = []
                for (let j=0;j<this.extract[i].length; j++){
                    if(this.extract[i][j] != '')
                    filtered[i].push(this.extract[i][j])
                }
            }
            this.smallestDiff(filtered)
        })
    }

    smallestDiff = (filtered) => {
    let ans;
    let diff = 100;
    for (let i = 0;  i< filtered.length; ++i) {
        for (let j=0;j<filtered[i].length; j++){
            if (Math.abs(filtered[i][this.rowDiff1]-filtered[i][this.rowDiff2])<diff){
                diff = Math.abs(filtered[i][this.rowDiff1]-filtered[i][this.rowDiff2])
                ans = filtered[i][this.ansRow]
            }
        }
    }
    console.log(ans)
    }
}

// x = new readingDatFile('weather.dat', 2, 1, 0)
// x.data()
module.exports = readingDatFile